import { Container, Carousel, Button } from 'react-bootstrap'
import { Link, NavLink, useHistory } from 'react-router-dom'
// import imahe from '../images/Product.jpg'

export default function Landing() {
	const history = useHistory()
	
	// const sale = "https://images.pexels.com/photos/3806753/pexels-photo-3806753.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
	// const newsletter = "https://images.pexels.com/photos/193003/pexels-photo-193003.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"

	const goToProducts = () => {
		history.push("/products")
	}

	return (
		// <Container className="d-flex justify-content-center">
		// 	<Carousel className="landingCarousel">
		// 	  <Carousel.Item>
		// 	    <img
		// 	      className="d-block w-100"
		// 	      src= {sale}
		// 	      alt="Check items on sale!"
		// 	    />
		// 	    <Carousel.Caption>
		// 	      <h3>Check items on sale!</h3>
		// 	    </Carousel.Caption>
		// 	  </Carousel.Item>
		// 	  <Carousel.Item>
		// 	    <img
		// 	      className="d-block w-100"
		// 	      src= {newsletter}
		// 	      alt="Subscribe to newsletter!"
		// 	    />
		// 	    <Carousel.Caption>
		// 	      <h3>Subscribe to newsletter!</h3>
		// 	    </Carousel.Caption>
		// 	  </Carousel.Item>
		// 	</Carousel>
		// </Container>

		
			<div className="landingDiv d-flex flex-row justify-content-center align-items-center">
				<button className='landingBtn' onClick={() => goToProducts()}>SHOP NOW</button>
			</div>
		

	)
}