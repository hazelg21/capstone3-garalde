import { useState, useEffect,useContext } from 'react'
import UserContext from '../UserContext'  

import { Container, Form, Button, InputGroup } from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Redirect, useHistory} from 'react-router-dom'




export default function CreateProduct() {
	const {user} = useContext(UserContext)
	const history = useHistory()	



	const [isActive, setIsActive] = useState(false);

	const [productName, setProductName] = useState('');
	const [productCategory, setProductCategory] = useState('');
	const [productVolume, setProductVolume] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');


	function addProduct(e) {
		e.preventDefault();


		fetch('https://stark-refuge-17906.herokuapp.com/api/products/create', {
			method:'POST',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				productCategory: productCategory,
				productVolume: productVolume,
				productDescription: productDescription,
				productPrice: productPrice
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data === false){
        		Swal.fire({
                  title: "Error",
                  icon: "error",
                  text: "An error has been encountered while saving."
                })
			}
			else {
				console.log(data)
				Swal.fire({
		          title: "Saved",
		          icon: "success",
		          text: "New Product has been saved"
		        })

				setProductName('')
				setProductCategory('')
				setProductVolume('')
				setProductDescription('')
				setProductPrice('')
			}
		})	
	}


	useEffect(() => {
		if(productName !== '' && productCategory !== 'Select category' && productVolume !== '' && productDescription !== '' && productPrice > 0 ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [productName, productCategory, productVolume, productDescription, productPrice])




	return (
		(user.id === null || user.isAdmin === false ) ?
            <Redirect to="/" />
        :
		<Container className="formContainer mt-3 p-4 w-75">
			<h1>New Product</h1>
			<br/>
			<Form className="mt-3" onSubmit={(e) => addProduct(e)}>
			  	<Form.Group className="mb-3" controlId="productName">
				    <Form.Label>Product Name</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Product Name" 
				    	value = {productName}
				    	onChange = { e => setProductName(e.target.value)}
				    	required 
				    />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="productCategory">
				    <Form.Label>Category</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				   
				    <Form.Select 
				    	onChange = { e => setProductCategory(e.target.value)}
				    	required
				    >
				      <option>Select Category</option>
				      <option value="Shampoo">Shampoo</option>
				      <option value="Conditioner">Conditioner</option>
				      <option value="Styler">Styler</option>
				    </Form.Select>
			    </Form.Group>



			    <Form.Group className="mb-3" controlId="productVolume">
				    <Form.Label>Volume</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Volume" 
				    	value = {productVolume}
				    	onChange = { e => setProductVolume(e.target.value)}
				    	required 
				    />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="productDescription">
				    <Form.Label>Description</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Description" 
				    	value = {productDescription}
				    	onChange = { e => setProductDescription(e.target.value)}
				    	required 
				    />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="productPrice">
				    <Form.Label>Price</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <InputGroup className="mb-3">
				        <InputGroup.Text>&#8369;</InputGroup.Text>
				        <Form.Control 
				        	type="number" 
				        	placeholder="Enter Price" 
				        	value = {productPrice}
				        	onChange = { e => setProductPrice(e.target.value)}
				        	required 
				        />
				      </InputGroup>
			    </Form.Group>
			

			  { isActive ? 
			  		<Button variant="success" type="submit" id="submitBtn" className="my-3 w-100 rounded-pill">
			  		  Save
			  		</Button>
			  	:
				  	<Button variant="danger" type="submit" id="submitBtn" className="my-3 w-100 rounded-pill" disabled>
				  	  Save
				  	</Button>
			  }
			</Form>
		</Container>
	)
}