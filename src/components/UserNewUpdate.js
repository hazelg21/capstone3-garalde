import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'

import { Container, Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Redirect, useHistory, useLocation } from 'react-router-dom'


export default function UserInfo() {
    const { user } = useContext(UserContext)
    const history = useHistory()
    const location = useLocation()
    const [isRegister, setIsRegister] = useState(true);
    const [formHeader, setFormHeader] = useState('Register');

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    const [mChecked, setMChecked] = useState("");
    const [fChecked, setFChecked] = useState("");

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [gender, setGender] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [deliveryAddress, setAddress] = useState('');

    function registerUser(e) {
        e.preventDefault();

        fetch('https://stark-refuge-17906.herokuapp.com/api/users/checkEmail', {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data === true) {
                    Swal.fire({
                        title: "Email is already registered",
                        icon: "error",
                        text: "Please use a different email."
                    })
                }
                else {
                    fetch('https://stark-refuge-17906.herokuapp.com/api/users/register', {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            age: age,
                            gender: gender,
                            email: email,
                            password: password2,
                            mobileNo: mobileNo,
                            deliveryAddress: deliveryAddress
                        })
                    })
                        .then(response => response.json())
                        .then(data => {
                            Swal.fire({
                                title: "Register Successful",
                                icon: "success",
                                text: "Login now to start adding items to your cart!"
                            })

                            clearFields()

                            history.push("/login")

                        })

                }
            })
    }

    const confirmUpdate = () => {
        Swal.fire({
            icon: 'question',
            title: 'Update Information?',
            inputLabel: 'Enter your password to confirm',
            input: 'password',
            inputPlaceholder: 'Enter your password',
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off'
            }
        }).then(result => {
            if (result.isConfirmed && result.value !== '') {
                updateInfo(result.value)
            }
        })
    }

    const updateInfo = (confirmPassword) => {
        fetch('https://stark-refuge-17906.herokuapp.com/api/users/updateInfo', {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                age: age,
                gender: gender,
                mobileNo: mobileNo,
                deliveryAddress: deliveryAddress,
                password: confirmPassword
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data) {
                    let timerInterval
                    Swal.fire({
                        title: 'Saved',
                        icon: 'success',
                        timer: 1200,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(result => {
                        clearFields()
                        history.push("/profile")
                    })
                }
                else {
                    Swal.fire('Authentication Failed', 'Please check the provided password', 'error')
                }
            })
    }

    const clearFields = () => {
        // Clear input fields
        setEmail('');
        setPassword1('');
        setPassword2('');
        setFirstName('');
        setLastName('');
        setAge('');
        setGender('');
        setMobileNo('');
        setAddress('');
    }

    const [decline, setDecline] = useState('')
    useEffect(() => {
        if (localStorage.getItem('token') !== null && location.pathname === "/register") {
            //logged in but trying to access register page
            // console.log("logged in but trying to access register page")
            setDecline(true)
        } else if (localStorage.getItem('token') === null && location.pathname === "/profile/updateInfo") {
            //logged OUT but trying to access update page
            // console.log("logged OUT but trying to access update page")
            setDecline(true)
        }
        else {
            setDecline(false)
        }
    }, [location.pathname])

    useEffect(() => {
        if (location.pathname === "/register") {
            setIsRegister(true)
            setFormHeader('Register')
        }
        else {
            setIsRegister(false)
            setFormHeader('Update User Information')
            loadForm()
        }
    }, [location.pathname, decline])

    const loadForm = () => {
        if (decline === false) {
            fetch('https://stark-refuge-17906.herokuapp.com/api/users/details', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
                .then(response => response.json())
                .then(data => {
                    setFirstName(data.firstName);
                    setLastName(data.lastName);
                    setAge(data.age);
                    setGender(data.gender);
                    setMobileNo(data.mobileNo);
                    setAddress(data.deliveryAddress);
                    if (data.gender === "Male") {
                        setMChecked("checked")
                    } else { setFChecked("checked") }
                })
        }
    }

    useEffect(() => {
        if (isRegister && (email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && age > 0 && gender !== '' && deliveryAddress !== '') && (password1 === password2) && (mobileNo.length === 11)) {
            setIsActive(true);
        }
        else if ((isRegister === false) && (firstName !== '' && lastName !== '' && age > 0 && gender !== '' && deliveryAddress !== '') && (mobileNo.length === 11)) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [email, password1, password2, firstName, lastName, age, gender, mobileNo, deliveryAddress])



    return (
        (decline) ?
            <Redirect to="/" />
            :
            //allow access
            <Container className="w-75 mt-4 px-5 py-3 formContainer">
                <h1>{formHeader}</h1>
                <br />
                <Form className="mt-3" onSubmit={(e) => registerUser(e)}>
                    <Form.Group className="mb-3" controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Text className=" text-danger">
                            &nbsp;*
                        </Form.Text>
                        <Form.Control
                            type="text"
                            placeholder="Enter First Name"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Text className=" text-danger">
                            &nbsp;*
                        </Form.Text>
                        <Form.Control
                            type="text"
                            placeholder="Enter Last Name"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <div className="row justify-content-between">
                        <div className="col-md-5 my-4 order-1">
                            <Form.Group className="mb-3" controlId="age">
                                <Form.Label>Age</Form.Label>
                                <Form.Text className=" text-danger">
                                    &nbsp;*
                                </Form.Text>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter Age"
                                    value={age}
                                    onChange={e => setAge(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </div>

                        <div className="col-md-5 my-4 order-2">
                            <Form.Group className="mb-3" controlId="gender">
                                <Form.Label>Gender</Form.Label>
                                <Form.Text className=" text-danger">
                                    &nbsp;*
                                </Form.Text>
                                <div key="inline-radio" className="mb-3">
                                    <Form.Check
                                        inline
                                        label="Male"
                                        name="group1"
                                        type="radio"
                                        id="inline-radio-1"
                                        value="Male"
                                        checked={mChecked}
                                        onClick={e => {
                                            setMChecked("checked")
                                            setFChecked("")
                                        }}
                                        onChange={e => { setGender(e.target.value) }}
                                        required
                                    />
                                    <Form.Check
                                        inline
                                        label="Female"
                                        name="group1"
                                        type="radio"
                                        id="inline-radio-2"
                                        value="Female"
                                        checked={fChecked}
                                        onClick={e => {
                                            setMChecked("")
                                            setFChecked("checked")
                                        }}
                                        onChange={e => { setGender(e.target.value) }}
                                        required
                                    />
                                </div>
                            </Form.Group>
                        </div>
                    </div>
                    <Form.Group className="mb-3" controlId="mobileNo">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Text className=" text-danger">
                            &nbsp;*
                        </Form.Text>
                        <Form.Control
                            type="number"
                            placeholder="Enter Mobile Number (11 digits)"
                            value={mobileNo}
                            onChange={e => setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="deliveryAddress">
                        <Form.Label>Delivery address</Form.Label>
                        <Form.Text className=" text-danger">
                            &nbsp;*
                        </Form.Text>
                        <Form.Control
                            type="text"
                            placeholder="Enter Delivery Address"
                            value={deliveryAddress}
                            onChange={e => setAddress(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <br />
                    {(!isRegister) ?
                        <>
                            {isActive ?
                                <Button variant="success" onClick={() => confirmUpdate()} id="submitBtn" className="my-3 w-100 rounded-pill">
                                    Update
                                </Button>
                                :
                                <Button variant="danger" id="submitBtn" className="my-3 w-100 rounded-pill" disabled>
                                    Update
                                </Button>
                            }
                        </>
                        :
                        <>
                            <Form.Group className="mb-3" controlId="userEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Text className=" text-danger">
                                    &nbsp;*
                                </Form.Text>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter Email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label>Password</Form.Label>
                                <Form.Text className=" text-danger">
                                    &nbsp;*
                                </Form.Text>
                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    value={password1}
                                    onChange={e => setPassword1(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password2">
                                <Form.Label>Verify Password</Form.Label>
                                <Form.Text className=" text-danger">
                                    &nbsp;*
                                </Form.Text>
                                <Form.Control
                                    type="password"
                                    placeholder="Verify Password"
                                    value={password2}
                                    onChange={e => setPassword2(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {isActive ?
                                <Button variant="success" type="submit" id="submitBtn" className="my-3 w-100 rounded-pill">
                                    Submit
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" className="my-3 w-100 rounded-pill" disabled>
                                    Submit
                                </Button>
                            }
                        </>
                    }
                </Form>
            </Container>
    )
}