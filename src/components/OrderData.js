import PropTypes from 'prop-types'
import { tr, Button } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import Swal from 'sweetalert2'
import { Fragment, useState, useEffect } from 'react'


// All Orders
export default function OrderData({orderProp}) {

	const { _id, userId, productId, productQty, productPrice,  totalAmount, isPaid, purchasedOn, deliveryStatus, isProcessed } = orderProp

	const history = useHistory()

	const processOrder = () => {
		Swal.fire({
		  title: 'Set order as Processed?',
		  text: "You won't be able to revert this.",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Process'
		}).then((result) => { 
			if (result.isConfirmed) {
				fetch('https://stark-refuge-17906.herokuapp.com/api/orders/processOrder', {
					method:'PUT',
					headers: {
						"Content-Type":"application/json",
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						userId:userId,
						orderId:_id,
					})
				})
				.then(response => response.json())
				.then(data => {
					if (data === false){
		        		Swal.fire({
		                  title: "Error",
		                  icon: "error",
		                  text: "An error has been encountered"
		                })
					}
				})
			}
		})
	}

	const viewOrder = () => {
		history.push(`/orders/${_id}`)
	}




	return (
	    <tr>
      	 	<td><Button variant="secondary" type="submit" className="w-100 rounded-pill text-white my-1" onClick={() => viewOrder()}>
	      	 	 Details
	      	 	</Button></td>
	      	<td>{_id}</td>
	      	<td>{totalAmount}</td>

	      	{(isPaid) ?
  	      		<td>Paid</td>
  	      		:
  		    	<td className="text-danger">Pending</td>
  	 		}

  	 		<td>{deliveryStatus}</td>

  	      	{(isProcessed) ?
  	      		<td>Processed</td>
  	      		:
  		    	<td className="text-danger">Open</td>
  	 		}

	      	{(isProcessed) ?
	      	 	<td>&nbsp;</td>
	      		:
	 			<td>
			      	<Button variant="warning" type="submit" className="w-100 rounded-pill text-white my-1" onClick={() => processOrder()}>Processed</Button>
 	 			</td>
		    }



	 	 	
	    </tr>
	)
}