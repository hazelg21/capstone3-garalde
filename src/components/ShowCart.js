import PropTypes from 'prop-types'
import { tr, Button, Form } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import Swal from 'sweetalert2'
import { Fragment, useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'  


export default function ShowCart({cartProp, idx}) {

	const history = useHistory()
	const { updateCart } = useContext(UserContext)
	const { _id, productId, productName, productCategory, productVolume, productPrice, productQty, orderId, isCheckedOut, isProcessed } = cartProp

	// const { productName, itemId, productId, productPrice, productQty, orderId, isCheckedOut, isProcessed } = cartProp

	const updateQty = (newQty) => {
		
		if (newQty !== 0) {
			fetch('https://stark-refuge-17906.herokuapp.com/api/users/updateQty', {
				method:'PUT',
				headers: {
					"Content-Type":"application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					index:idx,
					productQty: newQty
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data === false){
	        		Swal.fire({
	                  title: "Error",
	                  icon: "error",
	                  text: "An error has been encountered while saving."
	                })
				}
				else {
					updateCart(true)
				}
				
			})
		}
		else{
			removeItem(idx)
		}
	}	


	const removeItem = () => {

		Swal.fire({
		  title: 'Remove item?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Remove'
		}).then((result) => {
		  if (result.isConfirmed) {
			fetch('https://stark-refuge-17906.herokuapp.com/api/users/removeItem', {
					method:'PUT',
					headers: {
						"Content-Type":"application/json",
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						index:idx
					})
				})
				.then(response => response.json())
				.then(data => {
					if (data === false){
		        		Swal.fire({
		                  title: "Error",
		                  icon: "error",
		                  text: "An error has been encountered"
		                })
					}
					else {
						updateCart(true)
					}
				})
		  }
		})

	}

	const liveView = () => {
		history.push(`/products/${productId}`)
	}

	return (
			
	    <tr>
	    	<td><Button variant="secondary" type="submit" className="w-100 rounded-pill text-white my-1" onClick={() => liveView()}>Live View</Button></td>
	    	
	      	<td>{productName}</td>
	      	<td>{productCategory}</td>
	      	<td>{productVolume}</td>
	      	<td>&#8369; {productPrice}</td>
	      	<td className="text-center">
		      	{ (isCheckedOut) ?
		      		<strong>{productQty}</strong>
		      		:
		      		<>
			      	 	<Button variant="secondary" type="submit" className="w-10 square text-white mx-3" onClick={() => updateQty(productQty-1)}>
				      	 	 <strong>-</strong>
				      	</Button>
				      	<Form.Label><strong>{productQty}</strong></Form.Label> 	
					    <Button variant="secondary" type="submit" className="w-10 Square text-white mx-3" onClick={() => updateQty(productQty+1)}>
				      		<strong>+</strong>
				      	</Button>
				    </>  	
		      	}
	      	</td>

	      	<td>&#8369; {productPrice*productQty}</td>

	      		{ (isCheckedOut) ?
		      		 isProcessed ?	<td><Form.Label>For Delivery</Form.Label></td> : <td><Form.Label>Preparing your order</Form.Label></td>
		      		:
		      		
			      	<td><Button type="submit" variant="danger" className="btn btn-link text-danger removeLink w-10 Square mx-3" onClick={() => removeItem()}>
			      		<strong>Remove</strong>
			      	</Button></td>	 
			    }  	
	    </tr>
	)
}