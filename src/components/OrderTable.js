import Container from 'react-bootstrap/Container'
import { Table, thead, tbody, tr, th ,td, Button, Form } from 'react-bootstrap'

export default function OrderTable({orderListProp}) {

	const { productId, productName, productCategory, productVolume, productQty, productPrice} = orderListProp


	return (
	      

		<tr >
		    <td>{orderListProp.productId}</td>
		    <td>{orderListProp.productName}</td>
		    <td>{orderListProp.productCategory}</td>
		    <td>{orderListProp.productVolume}</td>
		    <td>{orderListProp.productQty}</td>
		    <td>&#8369; {orderListProp.productPrice}</td>
		    <td>&#8369; {orderListProp.productPrice*orderListProp.productQty}</td>
	 	</tr>		


	     
	) //return
}