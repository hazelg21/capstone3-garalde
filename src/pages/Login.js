import Container from 'react-bootstrap/Container'
import {Form, Button } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { Fragment, useState, useEffect,useContext } from 'react'
import UserContext from '../UserContext'  
import Swal from 'sweetalert2'
import { Redirect  } from 'react-router-dom'


export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	const {user, setUser, retrieveUserDetails } = useContext(UserContext)

	function authenticate(e){
		e.preventDefault();

		const authFailed = () => {
			Swal.fire({
			  title: "Authentication Failed",
			  icon: "error",
			  text: "Please check your login details and try again."
			})
		}

		fetch('https://stark-refuge-17906.herokuapp.com/api/users/checkEmail',{
			method:'POST',
			headers: {
				"Content-Type":"application/json",
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(response => response.json())
		.then(data => {
		    
		    if(data === false) {
		    	authFailed()
		    }
		    else {
    			fetch('https://stark-refuge-17906.herokuapp.com/api/users/login', {
    	            method: 'POST',
    	            headers: {
    	                'Content-type':'application/json'
    	            },
    	            body: JSON.stringify({
    	                email:email,
    	                password:password
    	            })
    	        })
    	        .then(response => response.json())
    	        .then(data => {
    	            // console.log(`data access: ${data.accessToken}`)
    	            if (typeof data.accessToken !== "undefined") {
    	                localStorage.setItem('token', data.accessToken);
    	                retrieveUserDetails(data.accessToken);

    	                Swal.fire({
    	                  title: "Login Successful",
    	                  icon: "success",
    	                  text: "Enjoy Shopping!",
    	                  showConfirmButton: false,
    	                  timer: 1000
    	                })

    	                setEmail('');
    			        setPassword('');
    	            }
    	            else {
    	                authFailed()
    	            }
    	        })
		    }
		})

	}


	useEffect (()=> {
		 if(email !== '' && password !== ''){
	        setIsActive(true);
	    }
	    else{
	        setIsActive(false);
	    }
	}, [email, password])



	return (
		(user.id !== null) ?
			<Redirect to="/" />
		:
		<Container className="w-50 mt-4 px-5 py-4 formContainer">
			<h1 className="formHeader">Login</h1>
			<br/>
			<Form onSubmit={(e) => authenticate(e) }>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter Email" 
			    	value={email}
			    	onChange={(e) => setEmail(e.target.value)}
			    	required
			    />


			  </Form.Group>
			  <br/>
			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
				    type="password" 
				    placeholder="Enter Password" 
				    value={password}
				    onChange={(e) => setPassword(e.target.value)}
				    required

			    />
			  </Form.Group>

			  { isActive ?
				  <Button variant="success" type="submit" className="mt-3 w-100 rounded-pill">
				    Sign In
				  </Button>
				  :
				  <Button variant="danger" type="submit" className="mt-3 w-100 rounded-pill" disabled>
				    Sign In
				  </Button>
			  }

			</Form>
			<br/>	
		  <Link as={Link} to="/register">Not yet registered? Sign Up Now!</Link>
		</Container>
		
	)
}